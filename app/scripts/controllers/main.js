'use strict';

angular.module('hashtagApp')
  .controller('MainCtrl', function ($scope, $http, $interval, $timeout) {

    var pending = [];
    var shown = [];
    $scope.loading = true;

    $scope.bufferSize = 30;

    $scope.pendingCount = function () {
      return pending.length;
    };

    var isNew = function (media, pending, shown) {
      var isQueud = _.findWhere(pending, {id:media.id});
      var hasShown = _.findWhere(shown, {id:media.id});
      if (isQueud == undefined && hasShown == undefined) {
        return true;
      } else {
        return false;
      }
    };

    var updateMedia = function (lastId) {
      $http.get('/api/tag').success(function(response) {
        $scope.error = false;
        _.each(response.medias, function (media, i, medias) {

         if( isNew(media, pending, shown) ) {
          var img = new Image();

          img.onload = function () {
            pending.unshift(media);
          };

          img.src = media.images.low_resolution.url.replace('http://', '//');;

         }

        });

      }).error(function () {
        $scope.loading = false;
        $scope.error = true;
      });
    };

    $scope.switcher = function() {
      var lastImage = pending.pop();
      var nextImage = pending[pending.length-1];
      // window.requestAnimationFrame(function () {
      if(pending.length > $scope.bufferSize) {
        $scope.bufferSize = 10;
        $scope.loading = false;
        shown.unshift(lastImage);
        $scope.featuredLast = shown[0].images.low_resolution.url.replace('http://', '//');
        $scope.featured = pending[pending.length-1].images.low_resolution.url.replace('http://', '//');

      } else {
        $scope.loading = true;
      }

      // });
      $timeout($scope.switcher, 1000/6);
    };

    $scope.switcher();

    $interval(function () {
      if(pending.length < 100) {
        updateMedia();
      }
    }, 300)
  });
